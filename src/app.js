const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const dotenv = require('dotenv');

const routerApi = require('./routes/api');

const { checkConnection } =require('./libraries/database');

dotenv.config();
(async function(){
    await checkConnection();

    const app = express();

    const jsonParser = bodyParser.json();
    const urlParser = bodyParser.urlencoded({ extended: false });
    
    app.use(cors('*'));

    app.use(jsonParser);
    app.use(urlParser);
    
    app.use('/api/v1', routerApi());
    
    // 404
    app.use(function (req, res, next) {
        res.status(404).send('Not Found');
    });
    
    // 500
    app.use(function (err, req, res, next) {
        if (err) {
            console.error(err.stack);
        }
    
        res.status(500).send('Something broke!');
    });
    
    
    const port = process.env.PORT || 3000;

    app.listen(port, function () {
      console.log(`Client App running on http://localhost:${port}`);
    });
    
})();
